var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
var cssnano = require('gulp-cssnano');

var path = {
    public: './public/',
    build: './public/build/',
    assets: './assets/',
    stylesheets: './assets/stylesheets/',
    javascripts: './assets/javascripts/'
};

var env = process.env.NODE_ENV || 'development';

if (env == 'development') {
    livereload = require('livereload');
    server = livereload.createServer();
    server.watch(path.public);
}

gulp.task('styles', function () {
    gulp.src(path.stylesheets + '*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass.sync({
            // outputStyle: 'compressed'
        }).on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        // .pipe(cssnano())
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(path.public));
});

gulp.task('scripts', function () {
});

gulp.task('watch', ['styles', 'scripts'], function () {
    gulp.watch(path.stylesheets + '**', ['styles']);
    gulp.watch(path.javascripts + '**', ['scripts']);
});

gulp.task('default', ['styles', 'scripts']);
